package sumando;

import java.util.Scanner;

public class Sumando {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero:");
		int num1 = input.nextInt();

		System.out.println("Introduce un numero:");
		int num2 = input.nextInt();

		int suma = num1 + num2;

		System.out.println("El resultado es " + suma);

		input.close();

	}

}
